# python3

import sys

from bot.game import Game

from bot.bot import Bot


def __main__():
    bot = Bot()
    game = Game()
    game.run(bot)

__main__()
