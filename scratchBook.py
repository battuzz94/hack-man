#SCRATCH BOOK

#Here above any idea for the development of the project ;)

#CONTEXT

#Environment:
#
#Map: the map has a total of 280 cells, 154 free and 126 blocked.
#     A character could stop or go through in each of them, if it's not blocked.
#     There are a total of 24 nodes in the map. Node means cell where there is
#     a route' biforcation. Max link lenght = 10 cells // Min link lenght = 1 cell
#Time:Each user has 0,5 second to decide where he can go, and the game time is
#     200 steps for each UserBot

#The game is composed by 4 characters:
#
#UserBot: this bot have to take as many snippet of code as possible
#
#Snippets: these tokens appears randomly in the map, every 1+8t steps
#
#Bugs: The bug can be considered as the malus of the game. They appear each
#      5 snippets eaten in the same position and then they move randomly in the map
#
#Swords: A sword can be considered as a bonus for the userBot, in fact
#        it changes (the "State" and) the behavior when he meet a bug or another
#        UserBot on the same cell. Each 8 snippets eaten a sword appears randomly
#        in the map

#When there are two character in the same cell we can observe different behavior:
#_USERBOT:
#UserBot-UserBot: Nothing
#UserBot-Bug: -4 snippets
#UserBot-Sword: change status UserBot(s)
#UserBot-Snippet: +1 Snippet

#_USERBOT_WITH_SWORD
#UserBot(s)-UserBot: Userbot-> -4 Snippets distributed around the map
#UserBot1(s)-UserBot2(s): UserBot1(s) -> -4 Snippets && UserBot2(s) -> -4 Snippets
#UserBot(s)-Bug: Bug dies

#After each crash the sword will be lost


#POSSIBLE ALGORITHMS
#
#- Find and head always to the closest snippet of code 
#
