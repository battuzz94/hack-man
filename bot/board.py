import copy
import sys

from collections import defaultdict

from bot.node import Node
from bot.constants import *


class Board:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.cell = [[[Node(row, col, EMPTY)] for col in range(0, width)] for row in range(0, height)]
        self.code_coordinates = []  # List of coordinates for each code snippet position

        self.distances = defaultdict(dict)      # Distance between all pairs of node
        self._next = defaultdict(dict)          # Used to reconstruct the min path from a pair of node

    def parse_cell_char(self, players, row, col, char):
        result = -1
        if char == S_PLAYER1:
            players[0].row = row;
            players[0].col = col;
        elif char == S_PLAYER2:
            players[1].row = row;
            players[1].col = col;
        for (i, symbol) in CHARTABLE:
            if symbol == char:
                result = i
                break
        return result

    def parse_cell(self, players, row, col, data):
        cell = []
        for char in data:
            cell.append(self.parse_cell_char(players, row, col, char))

        if (CODE in cell):  # Add code coordinate to the list
            self.code_coordinates.append((row, col))

        return cell

    def parse(self, players, data):
        cells = data.split(',')
        # sys.stderr.write("num cells in field = " + str(len(cells)) + "\n")
        # sys.stderr.write("width = " + str(self.width) + "\n")
        # sys.stderr.write("height = " + str(self.height) + "\n")
        col = 0
        row = 0
        self.code_coordinates = []
        for cell in cells:
            if (col >= self.width):
                col = 0
                row += 1
            self.cell[row][col] = Node(row, col, self.parse_cell(players, row, col, cell))
            col += 1

    def _in_bounds(self, row, col):
        return row >= 0 and col >= 0 and col < self.width and row < self.height

    def in_bounds(self, row, col, direction = (0,0)):
        return self._in_bounds(row + direction[0], col + direction[1])

    def legal_moves(self, my_id, players):
        my_player = players[my_id]
        # sys.stderr.write("my player loc = " + str(my_player.row) + ", " + str(my_player.col) + "\n")
        result = []
        for ((o_row, o_col), order) in DIRS:
            t_row = my_player.row + o_row
            t_col = my_player.col + o_col
            # sys.stderr.write("Testing " + str(t_row) + ", " + str(t_col) + "\n")
            if (self.in_bounds(t_row, t_col)) and (not BLOCKED in self.cell[t_row][t_col].node_type):
                # sys.stderr.write("legal\n")
                result.append(((o_row, o_col), order))
            else:
                pass
                # sys.stderr.write("illegal\n")
        return result

    def output_cell(self, cell):
        done = False
        for (i, symbol) in CHARTABLE:
            if i in cell:
                if not done:
                    sys.stderr.write(symbol)
                done = True
                break
        if not done:
            sys.stderr.write("!")
            done = True

    def output(self):
        for row in self.cell:
            sys.stderr.write("\n")
            for cell in row:
                self.output_cell(cell)
        sys.stderr.write("\n")
        sys.stderr.flush()



    def _build_graph(self, game):
        # Add arcs between nodes
        for i in range(self.height):
            for j in range(self.width):
                if (self._is_feasible(i, j, game)):
                    for ((dr, dc), _) in DIRS:
                        r, c = i + dr, j + dc
                        if game.my_botid in self.cell[i][j].node_type:      # This would be the very next step of my bot
                            if self._is_good_solution(r, c, game):
                                self.cell[i][j].addAdjacentNode(self.cell[r][c])
                        elif self._is_feasible(r, c, game):                       # Any other connection in the map
                            self.cell[i][j].addAdjacentNode(self.cell[r][c])

    def _is_feasible(self, r, c, game):
        return self._in_bounds(r, c) and \
               (any(t in self.cell[r][c].node_type for t in (EMPTY, WEAPON, CODE, PLAYER1, PLAYER2)) or \
                (BUG in self.cell[r][c].node_type and game.my_player().has_weapon))

    def _is_good_solution(self, r, c, game):
        in_bound = self.in_bounds(r, c)

        if not in_bound or BLOCKED in self.cell[r][c].node_type:
            return False

        adjacent_cells = [self.cell[r+dr][c+dc] for (dr,dc),_ in DIRS if self._in_bounds(r+dr, c+dc)]

        no_bug_around = all(BUG not in node.node_type for node in adjacent_cells) and \
                        BUG not in self.cell[r][c].node_type
        ok_for_bugs = no_bug_around or game.my_player().has_weapon  # no bugs around or player has sword


        # No other player around or I have sword, but not both
        no_oth_player_around = all(game.other_botid not in node.node_type for node in adjacent_cells) and \
            game.other_player not in self.cell[r][c].node_type
        ok_for_oth_player = no_oth_player_around or not game.other_player().has_weapon

        feasible = BLOCKED not in self.cell[r][c].node_type

        return feasible and ok_for_bugs and ok_for_oth_player

    # Floyd Warshall algorithm - minimum distance from all pairs
    def compute_distances(self):

        nodes = [n for row in self.cell for n in row if not BLOCKED in n.node_type]   # Vector of all cells in the map

        for u in nodes:
            for v in nodes:
                self.distances[u][v] = INF

        for v in nodes:
            self.distances[v][v] = 0
            #self._next[v][v] = v

        for u in nodes:
            for v in u.adjacent:
                self.distances[u][v] = 1
                self._next[u][v] = v


        for k in nodes:
            for i in nodes:
                for j in nodes:
                    if self.distances[i][j] > self.distances[i][k] + self.distances[k][j]:
                        self.distances[i][j] = self.distances[i][k] + self.distances[k][j]
                        self._next[i][j] = self._next[i][k]

    # Get the LIST of directions from NODE u to NODE v
    def reconstruct_path(self, u, v):
        if self._next[u][v] is None:
            return []
        path = [u]
        while u != v:
            u = self._next[u][v]
            path.append(u)

        return path
