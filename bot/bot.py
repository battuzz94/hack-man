import random
import sys
from bot.constants import *


class Bot:
    def __init__(self):
        self.game = None
        self.board = None

    def setup(self, game):
        self.game = game
        self.board = game.field

    def do_turn(self):
        self.board._build_graph(self.game)

        r, c = (self.game.my_player().row, self.game.my_player().col)
        r2, c2 = (self.game.other_player().row, self.game.other_player().col)

        my_cell = self.board.cell[r][c]
        oth_cell = self.board.cell[r2][c2]

        dir = (0, 0)
        if len(self.board.code_coordinates) == 0:  # No codes available
            dir = self._move_to_map_center(my_cell)
        else:
            dir = self._choose_min_dist_snippet_far_from_oth(my_cell, oth_cell)
            if dir is None:  # No closer snippet found --> move to the closest snippet in general
                dir = self._choose_min_dist(my_cell)

        self.game.issue_order(DIRS_NAME[dir])

    def _move_to_map_center(self, my_cell):
        legal = my_cell.adjacent
        if len(legal) == 0:
            return (0, 0)
        else:  # we will try to reach the adjacent position of the other player, the closest one to the center of the map
            chosen = min(legal,
                              key=lambda p: ( (p.position[0] - MIDDLEHEIGHT)**2) + (p.position[1] - MIDDLEWIDTH)**2 )
            direction = (chosen.position[0] - my_cell.position[0], chosen.position[1] - my_cell.position[1])
            return direction

    def _move_random(self):
        legal = self.game.field.legal_moves(self.game.my_botid, self.game.players)
        if len(legal) == 0:
            return (0, 0)
        else:
            (chosen, _) = random.choice(legal)
            return chosen

    def _choose_min_dist_snippet_far_from_oth(self, my_cell, oth_cell):
        min_dist = INF
        min_node = None
        min_direction = (0, 0)

        for code_row, code_col in self.board.code_coordinates:
            code_cell = self.board.cell[code_row][code_col]

            (d_me_code, direction) = my_cell.distance_to(code_cell)
            (d_oth_code, _) = oth_cell.distance_to(code_cell)

            if 0 < d_me_code < min_dist and d_me_code < d_oth_code:
                min_dist = d_me_code
                min_node = code_cell
                min_direction = direction

        if min_node is None:  # No code is found
            return None
        else:
            # Return direction of fastest path to that snippet
            return min_direction

    def _choose_min_dist(self, my_cell):
        min_dist = INF
        min_direction = (0, 0)

        for code_row, code_col in self.board.code_coordinates:
            code_cell = self.board.cell[code_row][code_col]

            (d_me_code, direction) = my_cell.distance_to(code_cell)

            if d_me_code < min_dist:
                min_dist = d_me_code
                min_direction = direction

        if min_dist == INF:  # No code is found
            return None
        else:
            # Return direction of fastest path to that snippet
            return min_direction
