from collections import deque
from .constants import *

# Represent a cell in the board and a node in the built graph
class Node:
    def __init__(self, r, c, node_type):
        self.node_type = node_type
        self.probability_table = []  # New List where append distance data for a min_max search
        self.adjacent = []
        self.position = (r, c)

    def addAdjacentNode(self, node):
        self.adjacent.append(node)


    # Compute minimum distance to the specified node
    # Returns a tuple (distance, direction).
    # If it is not possible to reach node, distance will be equal to INF
    def distance_to(self, node):
        open_list = deque()
        closed_list = set()

        open_list.append((self, 0, None))         # Add tuple <node, distance, direction>

        while open_list:
            (chosen, dist, direction) = open_list.popleft()

            if chosen == node:
                if direction is None:
                    direction = (0,0)
                return (dist, direction)

            closed_list.add(chosen)

            for anode in chosen.adjacent:
                if anode not in closed_list:
                    dir = direction
                    if dir is None:
                        dir = tuple(a - b for a, b in zip(anode.position, chosen.position))
                    open_list.append((anode, dist+1, dir))


        return (INF, (0,0))


    def __repr__(self):
        return "Node(%s, %s)" % tuple(map(str, (self.node_type, self.position)))
